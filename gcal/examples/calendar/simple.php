<?php
require_once '../../src/Google_Client.php';
require_once '../../src/contrib/Google_CalendarService.php';
session_start();

$client = new Google_Client();
$client->setApplicationName("TrialSafe");

// Visit https://code.google.com/apis/console?api=calendar to generate your
// client id, client secret, and to register your redirect uri.
$client->setClientId('354408230630.apps.googleusercontent.com');
$client->setClientSecret('b_juWTpi_zg7nuGxFVVATyvW');
$client->setRedirectUri('http://localhost/trialSafe/2%20build/gcal/examples/calendar/simple.php');
$client->setDeveloperKey('AIzaSyCYpQBQoTi21RywXu1tY9V4XhygejJD6GA');
$client->setUseObjects(true);

$cal = new Google_CalendarService($client);

if (isset($_GET['logout']))
{
	unset($_SESSION['token']);
}

if (isset($_GET['code']))
{
	$client->authenticate($_GET['code']);
	$_SESSION['token'] = $client->getAccessToken();
	header('Location: http://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF']);
}

if (isset($_SESSION['token']))
{
	$client->setAccessToken($_SESSION['token']);
}

if ($client->getAccessToken())
{
	$calList = $cal->calendarList->listCalendarList();
	//print "<h1>Calendar List</h1><pre>" . print_r($calList, true) . "</pre>";
	
	$user_calendar_id = '2d29geb2ppia5cgugr1rkj755k@group.calendar.google.com'; //where am i storing the CalendarID per user account?
	
	try //does calendar already exist?
	{
		$calendar = $cal->calendars->get($user_calendar_id);
	}
	catch(Google_ServiceException $e)
	{
		if($e->getCode() == 404)//calendar doesn't exist
		{
			$new_cal = new Google_Calendar();
			$new_cal->setSummary('subscriptions');
			$new_cal->setTimeZone('America/New_York');
			$calendar = $cal->calendars->insert($new_cal);
		}
	}
	catch (Google_Exception $e)
	{
		// Other error.
		print "An error occurred: (" . $e->getCode() . ") " . $e->getMessage() . "\n";
	}
	
	echo $calendar_id = $calendar->getId();
	echo '<br><br>';
	
	die();
	// new reminder event
	$event = new Google_Event();
	$event->setSummary('Cancel subscription');
	$event->setLocation('Somewhere');
	$event->sendNotifications = true;
	
	//time start
	$start = new Google_EventDateTime();
	$start->setDateTime('2013-10-13T10:00:00.000-07:00');
	$event->setStart($start);
	
	//time end
	$end = new Google_EventDateTime();
	$end->setDateTime('2013-10-13T10:25:00.000-07:00');
	$event->setEnd($end);
	
	//remind user
	$reminder = new Google_EventReminders();
	$reminder->setUseDefault(false);
	$overrides = array("method"=> "popup", "minutes" => "0");
	$reminder->setOverrides(array($overrides));
	$event->setReminders($reminder);
	$createdEvent 					= $cal->events->insert($calendar_id, $event);
	
	//include us on event
	$attendee1 = new Google_EventAttendee();
	$attendee1->setEmail('ben@alche.my');
	$attendees 			= array($attendee1);
	$event->attendees 	= $attendees;
	$createdEvent 		= $cal->events->insert($calendar_id, $event);
	
	//new event's ID
	echo $createdEvent->getId();
	
	$_SESSION['token'] = $client->getAccessToken();
} else {
	$authUrl = $client->createAuthUrl();
	
	header("Location: $authUrl");
	die();
}