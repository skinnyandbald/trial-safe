  /************************************************************************************
  This is your Page Code. The appAPI.ready() code block will be executed on every page load.
  For more information please visit our docs site: http://docs.crossrider.com
*************************************************************************************/

appAPI.ready(function($) {
	appAPI.message.addListener(function(msg) {
		if(msg.action == 'userLogin')
		{
			// your code for user login
			//console.log('extension.js: log in user!');
			
			appAPI.db.setFromRemote(
									'http://localhost/trialSafe/2%20build/gcal/examples/calendar/static.php', // gets calendar id
		                            'calendar_id',            		// The database key name
		                            appAPI.time.daysFromNow(7),  	// optional: expiration
		                            responseLoaded
		                            );
		}
	});
	
	function responseLoaded(response, headers)
	{
		var calendar_id =  appAPI.db.get('calendar_id');// now that user has signed up, gCal will tell us their calendar id...
		
		// save their calendar_id & email to the local db
		// we should also notify server know that we have a new user... or do that earlier when doing the google calendar thing?
		//appAPI.db.set('calendar_id', gCal_id);
		
		// get gCal ID
		//var calendar_id = appAPI.db.get('calendar_id');
		
		console.log('da cal: ' + calendar_id);
		
		// Once user logged in send message to background to save bookmarkURL
		if( typeof calendar_id === 'string')
		{
			//console.log('extension.js: user is logged in!');
			appAPI.message.toBackground({action:'saveSubscription', appURL:msg.appURL});
			
			return;
		}
	}
});