/************************************************************************************
  This is your background code.
  For more information please visit our wiki site:
  http://docs.crossrider.com/#!/guide/scopes_background
*************************************************************************************/

appAPI.ready(function($) {
	var activeTabURL = null;

	// Keep track of activeTabs URL
	appAPI.tabs.onTabSelectionChanged(function(tabInfo)
	{
		activeTabURL = tabInfo.tabUrl;
	});
	
	// Configure button
	appAPI.browserAction.setResourceIcon('icon.png');
	appAPI.browserAction.onClick(function() {
		appAPI.db.removeAll();//reset for testing purposes
		
		// check whether user is logged in
		// if so, get their gCal feed's ID
	    var calendar_id = appAPI.db.get('calendar_id');
	    
	    console.log(appAPI.db.getList());
	    
	    // check whether user is logged in
	    // (we'd have their subscriptions Calendar_id)
	    var userLoggedIn = (calendar_id === null ? 0 : 1);
		
		if ( userLoggedIn == 0 )
		{
			console.log('user not logged in!');
			
			// Send message to extension scope to display login form
			appAPI.message.toActiveTab({action:'userLogin', appURL: activeTabURL});
			
			return;
		}
		
		console.log('came back to BG!');
		
		// User already logged in, save activeTab's URL
		saveSubscription(activeTabURL);
	});
		
	// Listen for message that user has logged in and save bookmarkURL
	appAPI.message.addListener(function(msg) {
		if (msg.action === 'saveSubscription')
			saveSubscription(msg);
	});
	
	// Common function to save bookmark URL
	function saveSubscription(msg) {
		// Your bookmark save function
		console.log('add new url: ' + msg.appURL);
	}
});